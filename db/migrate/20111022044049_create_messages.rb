class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :content
      t.string :user_id
      t.string :where
      t.string :when
      t.string :more
      
      t.timestamps
    end
  end
end

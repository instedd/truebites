class Message < ActiveRecord::Base
  belongs_to :User
  
  def self.create params
      keyword, offset = params[:body].split
      user = User.find_by_email params[:'x-remindem-user']
      return reply(self.invalid_author(keyword), :to => params[:from]) unless user

      schedule = user.schedules.find_by_keyword keyword

      return reply(self.no_schedule_message(keyword), :to => params[:from]) unless schedule
      return reply(self.invalid_offset_message(params[:body], offset), :to => params[:from]) unless offset.nil? || offset.looks_as_an_int?

      new_subscriber = self.create! :phone_number => params[:from], 
                                          :offset => offset ? offset : 0, 
                                          :schedule => schedule,
                                          :subscribed_at => DateTime.current.utc

      schedule.generate_reminders :for => new_subscriber

      [schedule.build_message(params[:from], schedule.welcome_message)]
    end
  
end

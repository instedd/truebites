class NuntiumController < ApplicationController
  before_filter :authenticate
  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == Nuntium::Config['incoming_username'] && password == Nuntium::Config['incoming_password']
    end
  end

  def receive_at
    # from: params''[:from] (the sender, like 'sms://2945922')
    # body: params[:body] (the message)

    # Read more about how this works: https://bitbucket.org/instedd/nuntium/wiki/Interfaces
    #render :text => "hallo from truebites: #{params[:body]}", :content_type => 'text/plain'
    
    ##store in database
    
    render :json => Message.create(params)
        
    #store from in the database with the id of message just stored...
    
  end
end
